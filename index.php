<?php
/**
 * Created by PhpStorm.
 * User: bob
 * Date: 23.08.16
 * Time: 11:41
 */


$input = '1110111000111101';
echo $input .'<br>';
bibit($input);

/**
 * Implements main functionality.
 */
function bibit($input){


//convert input string to array
  $elements_array = convert_to_array($input);
// get number of array's elements
  $elements_number = count($elements_array);
if ($elements_number == 1) return;
// transform to array with pyramid levels
  $pyramid_elements_key_map = elements_key_map($elements_number);

// generates small pyramids key map
  $small_pyramids_key_map = small_pyramids_key_map($pyramid_elements_key_map);

  $result = pyramids_transform($elements_array, $small_pyramids_key_map);

  $result_text = implode($result);

    if (count($result) > 1) {
      bibit($result_text);
    }
  return;
}

function pyramids_transform($e_array, $key_maps) {
  $need_reducing = array();
  foreach ($key_maps as $key_map) {
    $e_array = small_pyramid_transform($e_array, $key_map);
    $new_array = array($e_array[$key_map[0]], $e_array[$key_map[1]], $e_array[$key_map[2]], $e_array[$key_map[3]]);
    $new_array_str = implode($new_array);

    if (($new_array_str === '0000') || ($new_array_str === '1111')) {
      $need_reducing[] = TRUE;
    } else {
      $need_reducing[] = FALSE;
    }
  }
  echo implode($e_array).'<br>';

  if (!in_array(FALSE, $need_reducing)&&(count($e_array) >= 4)) {

    $new_string = '';
    foreach ($key_maps as $key_map) {
      $new_array = array($e_array[$key_map[0]], $e_array[$key_map[1]], $e_array[$key_map[2]], $e_array[$key_map[3]]);
      $array[] = (array_sum($new_array))? '1' : '0';

    }
    //$array = array_reverse($array);
    $new_string = implode($array);
    echo $new_string.'<br>';
    return $array;
  }

  return $e_array;
}

function small_pyramid_transform(&$e_array, $keys){

  $key_pattern = '';
  $array_for_pattern = array($e_array[$keys[3]],$e_array[$keys[2]],$e_array[$keys[1]],$e_array[$keys[0]]);
  $key_pattern = implode($array_for_pattern);
  $get_transform = transition_rules($key_pattern);
  $new_array = convert_to_array($get_transform);

  foreach ($new_array as $key => $value) {
    $count = count($new_array);
    $e_array[$keys[$count-$key]] = $value;
  }

  return $e_array;
}


function small_pyramids_key_map($main_key_map) {
  $small_pyramids_key_map = array();
  for ($i=count($main_key_map); $i>1; $i--) {
    if (!isEven($i)) continue;
    $new_level_pyramid_keys = construct_small_pyramids($main_key_map[$i], $main_key_map[$i-1]);
    $small_pyramids_key_map = array_merge($small_pyramids_key_map, $new_level_pyramid_keys);
  }
  return $small_pyramids_key_map;
}

function elements_key_map($count) {
  $level = 1;
  $level_keys_count = 1;
  $key_map[$level] =  $count;
  $keys_array = range($count-1, 1);
  while (count($keys_array) > 1) {
    $level++;
    $level_keys_count = $level_keys_count+2;
    $level_keys = array_splice($keys_array, 0, $level_keys_count);
    $key_map[$level] = $level_keys;
  }
  return $key_map;

}

function construct_small_pyramids($e_level, $ne_level) {

  $pyramid_upside_down = 0;
  $count = (count($e_level)-1);

  for ($i=$count; $i>0; $i--) {
    if (!isEven($e_level[$i])) continue;
    if ($pyramid_upside_down) {
      $small_pyramids[] = array(
        0 => $e_level[$i],
        1 => $e_level[$i] + count($ne_level) + 2,
        2 => $e_level[$i] + count($ne_level) + 1,
        3 => $e_level[$i] + count($ne_level),
      );
      $pyramid_upside_down = 0;
    } else {
      $small_pyramids[] = array(
        0 => $e_level[$i] + count($ne_level) + 1,
        1 => $e_level[$i] + 1,
        2 => $e_level[$i],
        3 => $e_level[$i] -1,
      );
      $pyramid_upside_down = 1;
    }
  }

  return $small_pyramids;
}

/**
 * Convert string into array with key begins from 1 and reversed keys.
 */
function convert_to_array($string) {

  $input_array = str_split($string);
  //$input_array = array_reverse($input_array);
  array_unshift($input_array, '');

  unset($input_array[0]);

  return $input_array;
}

/**
 * function applying transition rules.
 */

function transition_rules($num) {
  $rule = array(
    '0000' => '0000',
    '0001' => '1000',
    '0010' => '0001',
    '0011' => '0010',
    '0100' => '0000',
    '0101' => '0010',
    '0110' => '1011',
    '0111' => '1011',
    '1000' => '0100',
    '1001' => '0101',
    '1010' => '0111',
    '1011' => '1111',
    '1100' => '1101',
    '1101' => '1110',
    '1110' => '0111',
    '1111' => '1111',
  );
  return $rule[$num];
}

function isEven($value)
{
  return ($value%2 === 0);
}

